'use strict';

const assert = require('assert');
const fs = require('fs');
const path = require('path');

const yaml = require('js-yaml');

const load = (name) => {
	const filePath = path.join(__dirname, '..', '..', 'src', 'locales', name);
	return new Promise((resolve, reject) => {
			fs.readFile(filePath, 'utf8', (error, contents) => {
				if (error) {
					reject(error);
				}
				resolve(contents);
			});
		})
		.then(yaml.safeLoad);
};

const assertDict = (data) => {
	assert.equal(data.constructor, Object, 'Data is an Object');
	let count = 0;
	for (const [key, value] of Object.entries(data)) {
		count += 1;
		assert.equal(typeof value, 'string', `property "${key}" is a string`);
	}
	assert(count > 0, 'Data has at least one property');
};

const keys = async (name) => Object.keys(await load(name)).sort();

suite('locales', () => {
	let canonicalKeys;

	suiteSetup(async () => {
		canonicalKeys = await keys('en-us.yml');
	});

	suite('en-us', () => {
		test('shape', async () => {
			assertDict(await load('en-us.yml'));
		});
	});

	suite('en-pirate', () => {
		test('shape', async () => {
			assertDict(await load('en-pirate.yml'));
		});

		test('completeness', async () => {
			assert.deepEqual(await keys('en-pirate.yml'), canonicalKeys);
		});
	});
});
