#!/usr/bin/env node

const child_process = require('child_process');
const {createWriteStream} = require('fs');
const fs = require('fs').promises;
const http = require('http');
const https = require('https');
const path = require('path');

const {foodDataSource} = require('../package.json');

const jsonPath = process.argv[2];
const xlsxPath = jsonPath.replace(/\.json$/i, '.xlsx');

const exists = async (destination) => {
	let handle;

	try {
		handle = await fs.open(destination);
		return true;
	// Work around bug in JSHint
	// https://github.com/jshint/jshint/issues/3480
	// jshint -W137
	} catch ({}) {
	// jshint +W137
		return false;
	} finally {
		if (handle) {
			await handle.close();
		}
	}
};

const download = async (source, destination) => {
	console.log(`Downloading "${source}"...`);
	const get = new URL(source).protocol === 'https:' ? https.get : http.get;
	const destinationStream = createWriteStream(destination);

	return new Promise((resolve, reject) => {
		get(source, (response) => {
			response.pipe(destinationStream);
			response.on('end', resolve);
		}).on('error', reject);
	}).then(() => console.log('Download complete'));
};

const parse = (source, destination) => {
	console.log(`Parsing "${source}"...`);
	const parseScript = path.join(__dirname, 'parse-nutrition-data.js');
	const child = child_process.spawn('node', [parseScript, source]);
	child.stdout.pipe(createWriteStream(destination));

	return new Promise((resolve, reject) => {
		child.on('close', (code) => {
			if (code === 0) {
				resolve();
			} else {
				reject('Failed to parse');
			}
		});
	});
};

(async () => {
	await fs.mkdir(path.dirname(xlsxPath), { recursive: true });
	if (await exists(xlsxPath)) {
		console.log(`File named '${xlsxPath}' is present. Not downloading.`);
	} else {
		await download(foodDataSource, xlsxPath);
	}

	if (await exists(jsonPath)) {
		console.log(`File named '${jsonPath}' is present. Not parsing.`);
	} else {
		await parse(xlsxPath, jsonPath);
	}

	console.log('Done');
})().catch((error) => {
	console.error(error);
	process.exitCode = 1;
});
