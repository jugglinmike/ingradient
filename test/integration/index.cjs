'use strict';

const path = require('path');
const {Builder, By} = require('selenium-webdriver');

const Application = require('./application.cjs');
const build = require('./build.cjs');
const serve = require('./serve.cjs');

const root = path.join(__dirname, '..', '..', 'dist');

const timeout = 1000;

let server, driver;

suiteSetup(async function() {
	this.timeout(null);
	[server] = await Promise.all([
		serve(root),
		build()
	]);
	driver = await new Builder().forBrowser('firefox').build();
});

suiteTeardown(async () => {
	await driver && driver.quit();
	await server.close();
});

setup(async function() {
	await driver.get(server.address);
	this.application = new Application({driver, timeout});
	await this.application.ready();
});
