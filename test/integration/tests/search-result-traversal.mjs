'use strict';

import assert from 'assert';

import webdriver from 'selenium-webdriver';
const {By, Key} = webdriver;

suite('search result traversal', async function() {
	test('down', async function() {
		const application = this.application;

		await application.sendSearchKeys('traversal');
		await application.getSearchResults(4);

		await application.sendSearchKeys(Key.DOWN);

		assert.equal(await application.readFocused(), 'traversal a');

		await application.sendSearchKeys(Key.DOWN);

		assert.equal(await application.readFocused(), 'traversal b');

		await application.sendSearchKeys(Key.DOWN);

		assert.equal(await application.readFocused(), 'traversal c');

		await application.sendSearchKeys(Key.DOWN);

		assert.equal(await application.readFocused(), 'traversal d');

		await application.sendSearchKeys(Key.DOWN);

		assert.equal(await application.readFocused(), 'traversal a');
	});

	test('up', async function() {
		const application = this.application;

		await application.sendSearchKeys('traversal');
		await application.getSearchResults(4);

		await application.sendSearchKeys(Key.UP);

		assert.equal(await application.readFocused(), 'traversal d');

		await application.sendSearchKeys(Key.UP);

		assert.equal(await application.readFocused(), 'traversal c');

		await application.sendSearchKeys(Key.UP);

		assert.equal(await application.readFocused(), 'traversal b');

		await application.sendSearchKeys(Key.UP);

		assert.equal(await application.readFocused(), 'traversal a');

		await application.sendSearchKeys(Key.UP);

		assert.equal(await application.readFocused(), 'traversal d');
	});

	test('home', async function() {
		const application = this.application;

		await application.sendSearchKeys('traversal');
		await application.getSearchResults(4);

		await application.sendSearchKeys(Key.HOME);

		assert.equal(await application.readFocused(), 'traversal a');
	});

	test('end', async function() {
		const application = this.application;

		await application.sendSearchKeys('traversal');
		await application.getSearchResults(4);

		await application.sendSearchKeys(Key.END);

		assert.equal(await application.readFocused(), 'traversal d');
	});

	test('dismissal', async function() {
		const application = this.application;

		await application.sendSearchKeys('traversal');
		await application.getSearchResults(4);

		await application.sendSearchKeys(Key.ESCAPE);

		await application.getSearchResults(0);

		await application.sendSearchKeys('traversal');
		await application.getSearchResults(4);
	});
});
