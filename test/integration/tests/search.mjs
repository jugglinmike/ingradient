'use strict';

import assert from 'assert';

import webdriver from 'selenium-webdriver';
const {Key} = webdriver;

suite('search', async function() {
	test('search', async function() {
		const application = this.application;

		await application.getSearchResults(0);

		await application.sendSearchKeys('search');

		await application.getSearchResults(15);

		await application.sendSearchKeys(' ');

		await application.getSearchResults(15);

		await application.sendSearchKeys('two');

		await application.getSearchResults(11);

		await application.sendSearchKeys(' ');

		await application.getSearchResults(11);

		await application.sendSearchKeys('o');

		await application.getSearchResults(8);

		await application.sendSearchKeys('ne');

		await application.getSearchResults(8);

		await application.sendSearchKeys('X');

		await application.getSearchResults(0);

		await application.sendSearchKeys(Key.BACK_SPACE);

		await application.getSearchResults(8);
	});
});
