// jshint node: true
'use strict';
const fs = require('fs').promises;

const Asset = require('parcel-bundler/src/Asset.js');
const licenseChecker = require('license-checker');

module.exports = class extends Asset {
	constructor(...args) {
		super(...args);
		this.type = 'js';
	}

	async parse() {
		const start = process.cwd();
		const production = true;

		const projects = await new Promise((resolve, reject) => {
			licenseChecker.init({ start, production: true}, (error, data) => {
				if (error) {
					reject(error);
					return;
				}
				resolve(data);
			});
		});

		const formatted = Object.entries(projects)
				.map(([nameAndVersion, project]) => {
					const [, name, version] = nameAndVersion
						.match(/(.*)@([^@]+)$/);

					if (name === 'ingradient') {
						return null;
					}

					return {
						name,
						version,
						repository: project.repository,
						licenseName: project.licenses,
						licenseFile: project.licenseFile
					};
				})
				.filter((project) => !!project);

		return Promise.all(
			formatted.map(async (project) => {
				return {
					...project,
					license: {
						name: project.licenseName,
						text: await fs.readFile(project.licenseFile, 'utf-8')
					}
				};
			})
		);
	}

	generate() {
		return {
			js: `module.exports = ${JSON.stringify(this.ast)};`
		};
	}
};
