'use strict';

import assert from 'assert';

import webdriver from 'selenium-webdriver';
const {By, Key} = webdriver;

suite('solve', async function() {
	test('dismissal', async function() {
		const application = this.application;
		let ingredients;

		await application.sendSearchKeys('solve a');
		await application.getSearchResults(1);

		await application.sendSearchKeys(Key.DOWN);
		await application.sendSearchKeys(' ');
		await application.sendSearchKeys(Key.ESCAPE);

		await application.inputFact('calories', 200);

		ingredients = await application.readIngredients();

		assert.equal(ingredients['solve a'], '247g');

		await application.sendSearchKeys('solve b');
		await application.getSearchResults(1);

		await application.sendSearchKeys(Key.DOWN);
		await application.sendSearchKeys(' ');
		await application.sendSearchKeys(Key.ESCAPE);

		await application.inputFact('total fat', 4);

		ingredients = await application.readIngredients();

		assert.equal(ingredients['solve a'], '150g');
		assert.equal(ingredients['solve b'], '20g');

		await application.sendSearchKeys('solve c');
		await application.getSearchResults(1);

		await application.sendSearchKeys(Key.DOWN);
		await application.sendSearchKeys(' ');
		await application.sendSearchKeys(Key.ESCAPE);

		await application.inputFact('total sugars', 10);

		ingredients = await application.readIngredients();

		assert.equal(ingredients['solve a'], '155g');
		assert.equal(ingredients['solve b'], '15g');
		assert.equal(ingredients['solve c'], '36g');
	});
});
