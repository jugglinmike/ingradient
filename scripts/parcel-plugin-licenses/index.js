// jshint node: true

'use strict';

module.exports = (bundler) => {
	bundler.addAssetType('licenses', require.resolve('./asset.js'));
};
