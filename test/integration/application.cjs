'use strict';
const {By} = require('selenium-webdriver');

class TimeoutError extends Error {
	constructor(duration, message) {
		super(`Waited over ${duration} milliseconds for ${message}`);
	}
}

module.exports = class Application {
	constructor({driver, timeout}) {
		this.driver = driver;
		this.timeout = timeout;
	}

	async clickResult(number) {
		const options = await this.driver.findElements(
			By.css('[role=listbox] [role=option]')
		);

		if (number >= options.length) {
			throw new Error(
				`Attempted to click result number ${number}, but only ${options.length} options are present`
			);
		}

		const target = options[number];
		const wasSelected = await target.getAttribute('aria-selected');
		let start = Date.now();

		// Results transition into view, so the target may not be able to
		// receive a click immediately.
		while (true) {
			try {
				await options[number].click();
				break;
			// Work around bug in JSHint
			// https://github.com/jshint/jshint/issues/3480
			// jshint -W137
			} catch ({}) {
			// jshint +W137
				if (Date.now() - start > this.timeout) {
					throw new TimeoutError(
						this.timeout,
						`attempting to click result number ${number}`
					);
				}
			}
		}

		start = Date.now();

		while ((await this.driver.findElements(
			By.css('[role=listbox] [role=option]')
		)).length > 0) {
			if (Date.now() - start > this.timeout) {
				throw new TimeoutError(
					this.timeout,
					`result number ${number} to react to selection`
				);
			}
		}
	}

	async getSearchResults(count) {
		const start = Date.now();
		let results;

		do {
			if (Date.now() - start > this.timeout) {
				throw new TimeoutError(
					this.timeout, `${count} results to be present`
				);
			}

			results = await this.driver.findElements(
				By.css('[role=listbox] [role=option]')
			);
		} while (results.length !== count);

		// The UI takes a non-negligible amount of time to populate the text
		// in the results. For some reason.
		if (results.length) {
			do {
				if (Date.now() - start > this.timeout) {
					throw new TimeoutError(
						this.timeout, `${count} results to be present`
					);
				}
			} while (await results[0].getText() === '');
		}
		return results;
	}

	async inputFact(name, value) {
		const labels = await this.driver.findElements(By.css('label'));
		let target;

		for (const label of labels) {
			const text = await label.getText();
			if (text.trim().toLowerCase() === name) {
				if (target) {
					throw new Error(
						`Found multiple inputs for fact named "${name}"`
					);
				}
				const id = await label.getAttribute('for');
				target = await this.driver.findElement(By.css(`#${id}`));
			}
		}

		if (!target) {
			throw new Error(
				`Unable to locate input for fact named "${name}"`
			);
		}

		return target.sendKeys(value);
	}

	async readFocused() {
		const start = Date.now();
		const listbox = await this.driver.findElement(
			By.css('[role=listbox]')
		);
		let id;

		do {
			if (Date.now() - start > this.timeout) {
				throw new TimeoutError(
					this.timeout, 'search result to receive focus'
				);
			}

			id = await listbox.getAttribute('aria-activedescendant');
		} while (!id);

		const element = await this.driver.findElement(By.css(`#${id}`));
		return await element.getText();
	}

	async readIngredients() {
		const items = await this.driver.findElements(
			By.css('.ingredient-list-item')
		);

		const list = await Promise.all(items.map(async (item) => {
			const amountElement = await item.findElement(By.css('.amount'));
			const nameElement = await item.findElement(By.css('.food-name'));
			return {
				amount: await amountElement.getText(),
				name: await nameElement.getText()
			};
		}));

		return list.reduce((all, {name, amount}) => {
			all[name] = amount;
			return all;
		}, {});
	}

	async readSelected() {
		const selected = await this.driver.findElements(
			By.css('[role=listbox] [aria-selected=true]')
		);

		return Promise.all(selected.map((element) => element.getText()));
	}

	async ready() {
		const start = Date.now();
		const input = await this.driver.findElement(
			By.css('[role=combobox] input')
		);

		while (!await input.isEnabled()) {
			if (Date.now() - start > this.timeout) {
				throw new TimeoutError(
					this.timeout, 'application to be ready'
				);
			}
		}
	}

	async sendSearchKeys(terms) {
		const input = await this.driver.findElement(
			By.css('[role=combobox] input')
		);

		await input.sendKeys(terms);
	}
};
