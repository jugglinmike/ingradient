'use strict';

const assert = require('assert');

const solve = require('../../src/solve');

suite('solve', () => {
	test('sample', () => {
		assert.deepEqual(
			solve([
				[4, 8],
			]),
			[2]
		);
		assert.deepEqual(
			solve([
				[1, 2, 3],
				[4, 5, 6]
			]),
			[-1, 2]
		);
		assert.deepEqual(
			solve([
				[2, 1, -1, 8],
				[-3, -1, 2, -11],
				[-2, 1, 2, -3]
			]),
			[2, 3, -1]
		);
	});

	test('equation ordering does not match solution ordering', () => {
		assert.deepEqual(
			solve([
				[1, 1, 1, 6],
				[1, 1, 2, 9],
				[1, 2, 1, 8]
			]),
			[1, 2, 3]
		);
	});

	test('zero terms', () => {
		assert.deepEqual(
			solve([
				[0, 1, 1, 2],
				[1, 0, 1, 2],
				[1, 1, 0, 2]
			]),
			[1, 1, 1]
		);
		assert.deepEqual(
			solve([
				[0, 1, 1, 2],
				[1, 1, 0, 2],
				[1, 0, 1, 2]
			]),
			[1, 1, 1]
		);
		assert.deepEqual(
			solve([
				[1, 0, 1, 2],
				[0, 1, 1, 2],
				[1, 1, 0, 2]
			]),
			[1, 1, 1]
		);
		assert.deepEqual(
			solve([
				[1, 0, 1, 2],
				[1, 1, 0, 2],
				[0, 1, 1, 2]
			]),
			[1, 1, 1]
		);
		assert.deepEqual(
			solve([
				[1, 1, 0, 2],
				[1, 0, 1, 2],
				[0, 1, 1, 2]
			]),
			[1, 1, 1]
		);
		assert.deepEqual(
			solve([
				[0, 1, 1, 2],
				[1, 0, 1, 2],
				[1, 1, 0, 2]
			]),
			[1, 1, 1]
		);
	});

	test('superfluous equations', () => {
		assert.deepEqual(
			solve([
				[1, 1, 1, 3],
				[0, 1, 0, 1],
				[0, 0, 1, 1],
				[0, 2, 0, 2]
			]),
			[1, 1, 1]
		);

		assert.deepEqual(
			solve([
				[1, 1, 1, 3],
				[0, 0, 0, 0],
				[0, 0, 1, 1],
				[0, 2, 0, 2]
			]),
			[1, 1, 1]
		);
	});

	test('underconstrained', () => {
		const isUnderConstrainedError = (error) => {
			assert(error instanceof TypeError);
			assert.equal(error.code, 'UNDERCONSTRAINED');
			return true;
		};

		assert.throws(() => {
			solve([
				[1, 1, 2]
			]);
		}, isUnderConstrainedError);

		assert.throws(() => {
			solve([
				[1, 1, 2],
				[0, 0, 0]
			]);
		}, isUnderConstrainedError);

		assert.throws(() => {
			solve([
				[1, 1, 2],
				[2, 2, 4]
			]);
		}, isUnderConstrainedError);
	});

	test('overconstrained', () => {
		const isOverConstrainedError = (error) => {
			assert(error instanceof TypeError);
			assert.equal(error.code, 'OVERCONSTRAINED');
			return true;
		};

		assert.throws(() => {
			solve([
				[1, 2, 3],
				[4, 5, 6],
				[7, 8, 10]
			]);
		}, isOverConstrainedError);
	});
});
