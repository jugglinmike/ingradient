#!/usr/bin/env node

/**
 * Parse nutrition data in an input xlsx-formatted file, extract information
 * relevant for the application and output the result in JSON format to
 * standard out.
 *
 * This script is designed to accept input data as provided by the US
 * Department of Agriculture's Food and Nutrient Database for Dietary Studies
 * (FNDDS)
 *
 * https://www.ars.usda.gov/northeast-area/beltsville-md-bhnrc/beltsville-human-nutrition-research-center/food-surveys-research-group/docs/fndds/
 */
'use strict';

const xlsx = require('xlsx');

const workbook = xlsx.readFile(process.argv[2]);

const sheet = workbook.Sheets['Ingredient Nutrient Values'];

const ingredients = Object.create(null);

const nutrients = [
	'Energy',
	'Total Fat',
	'Fatty acids, total saturated',
	'Fatty acids, total monounsaturated',
	'Fatty acids, total polyunsaturated',
	'Cholesterol',
	'Sodium',
	'Carbohydrate',
	'Fiber, total dietary',
	'Sugars, total',
	'Protein',
];

let index = 3;
while (`A${index}` in sheet) {
	const id = sheet[`A${index}`].v;
	if (!(id in ingredients)) {
		ingredients[id] = {
			id,
			name: sheet[`B${index}`].v
		};
	}
	const nutrient = sheet[`D${index}`].v;
	if (nutrients.includes(nutrient)) {
		ingredients[id][nutrient] = sheet[`E${index}`].v;
	}
	index += 1;
}

const values = Object.values(ingredients)
	.map((ingredient) => [
		ingredient.id,
		ingredient.name,
		...nutrients.map((nutrient) => ingredient[nutrient])
	]);

console.log(JSON.stringify(values));
