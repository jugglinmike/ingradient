const http = require('http');
const url = require('url');
const fs = require('fs');
const path = require('path');

// maps file extention to MIME types
// full list can be found here: https://www.freeformatter.com/mime-types-list.html
const mimeType = {
	'.ico': 'image/x-icon',
	'.html': 'text/html',
	'.js': 'text/javascript',
	'.json': 'application/json',
	'.css': 'text/css',
	'.png': 'image/png',
	'.jpg': 'image/jpeg',
	'.wav': 'audio/wav',
	'.mp3': 'audio/mpeg',
	'.svg': 'image/svg+xml',
	'.pdf': 'application/pdf',
	'.zip': 'application/zip',
	'.doc': 'application/msword',
	'.eot': 'application/vnd.ms-fontobject',
	'.ttf': 'application/x-font-ttf',
};

const handle = (root, request, response) => {
	const parsedUrl = url.parse(request.url);

	// extract URL path
	// Avoid https://en.wikipedia.org/wiki/Directory_traversal_attack
	// e.g curl --path-as-is http://localhost:9000/../fileInDanger.txt
	// by limiting the path to current directory only
	const sanitizePath = path.normalize(parsedUrl.pathname)
		.replace(/^(\.\.[\/\\])+/, '');
	let pathname = path.join(root, sanitizePath);

	fs.exists(pathname, (exists) => {
		if (!exists) {
			// if the file is not found, return 404
			response.statusCode = 404;
			response.end(`File ${pathname} not found!`);
			return;
		}

		// if is a directory, then look for index.html
		if (fs.statSync(pathname).isDirectory()) {
			pathname += '/index.html';
		}

		// read file from file system
		fs.readFile(pathname, (err, data) => {
			if (err){
				response.statusCode = 500;
				response.end(`Error getting the file: ${err}.`);
			} else {
				const ext = path.parse(pathname).ext;
				// if the file is found, set Content-type and send data
				response.setHeader('Content-type', mimeType[ext] || 'text/plain');
				response.end(data);
			}
		});
	});
};

module.exports = (root) => {
	const server = http.createServer(handle.bind(null, root));
	const close = () => new Promise((resolve) => server.close(resolve));

	return new Promise((resolve, reject) => {
		server.listen(0, 'localhost', () => {
			const {port, address} = server.address();
			resolve({close, address: `http://${address}:${port}`});
		});
		server.on('error', reject);
	});
};
