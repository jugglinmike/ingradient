'use strict';

import assert from 'assert';

import webdriver from 'selenium-webdriver';
const {By, Key} = webdriver;

suite('search result selection', async function() {
	test('keyboard selection', async function() {
		const application = this.application;
		let ingredients;

		await application.sendSearchKeys('selection');
		await application.getSearchResults(5);

		assert.deepEqual(await application.readSelected(), []);

		await application.sendSearchKeys(Key.DOWN);
		await application.sendSearchKeys(' ');

		await application.getSearchResults(0);

		ingredients = await application.readIngredients();

		assert.deepEqual(ingredients, {'selection a': '?g'});

		await application.sendSearchKeys('selection');
		await application.getSearchResults(5);

		assert.deepEqual(await application.readSelected(), ['selection a']);

		await application.sendSearchKeys(Key.DOWN);
		await application.sendSearchKeys(Key.DOWN);
		await application.sendSearchKeys(Key.DOWN);
		await application.sendSearchKeys(' ');

		await application.getSearchResults(0);

		ingredients = await application.readIngredients();

		assert.deepEqual(ingredients, {
			'selection a': '?g',
			'selection c': '?g'
		});

		await application.sendSearchKeys('selection');
		await application.getSearchResults(5);

		assert.deepEqual(
			await application.readSelected(),
			['selection a', 'selection c']
		);

		await application.sendSearchKeys(Key.DOWN);
		await application.sendSearchKeys(Key.DOWN);
		await application.sendSearchKeys(' ');

		await application.getSearchResults(0);

		ingredients = await application.readIngredients();

		assert.deepEqual(ingredients, {
			'selection a': '?g',
			'selection b': '?g',
			'selection c': '?g'
		});

		await application.sendSearchKeys('selection');
		await application.getSearchResults(5);

		assert.deepEqual(
			await application.readSelected(),
			['selection a', 'selection b', 'selection c']
		);

		await application.sendSearchKeys(Key.DOWN);
		await application.sendSearchKeys(Key.DOWN);
		await application.sendSearchKeys(Key.DOWN);
		await application.sendSearchKeys(' ');

		await application.getSearchResults(0);

		ingredients = await application.readIngredients();

		assert.deepEqual(ingredients, {
			'selection a': '?g',
			'selection b': '?g'
		});

		await application.sendSearchKeys('selection');
		await application.getSearchResults(5);

		assert.deepEqual(
			await application.readSelected(),
			['selection a', 'selection b']
		);

		await application.sendSearchKeys(Key.DOWN);
		await application.sendSearchKeys(Key.DOWN);
		await application.sendSearchKeys(Key.UP);
		await application.sendSearchKeys(' ');

		await application.getSearchResults(0);

		ingredients = await application.readIngredients();

		assert.deepEqual(ingredients, {'selection b': '?g'});

		await application.sendSearchKeys('selection');
		await application.getSearchResults(5);

		assert.deepEqual(await application.readSelected(), ['selection b']);

		await application.sendSearchKeys(Key.DOWN);
		await application.sendSearchKeys(Key.DOWN);
		await application.sendSearchKeys(' ');

		await application.getSearchResults(0);

		ingredients = await application.readIngredients();

		assert.deepEqual(ingredients, {});

		await application.sendSearchKeys('selection');
		await application.getSearchResults(5);

		assert.deepEqual(await application.readSelected(), []);
	});

	test('mouse selection', async function() {
		const application = this.application;
		let ingredients;

		await application.sendSearchKeys('selection');
		await application.getSearchResults(5);

		assert.deepEqual(await application.readSelected(), []);

		await application.clickResult(4);

		ingredients = await application.readIngredients();

		assert.deepEqual(ingredients, {'selection e': '?g'});

		await application.sendSearchKeys('selection');
		await application.getSearchResults(5);

		assert.deepEqual(await application.readSelected(), ['selection e']);

		await application.clickResult(1);

		ingredients = await application.readIngredients();

		assert.deepEqual(ingredients, {
			'selection b': '?g',
			'selection e': '?g'
		});

		await application.sendSearchKeys('selection');
		await application.getSearchResults(5);

		assert.deepEqual(
			await application.readSelected(), ['selection b', 'selection e']
		);

		await application.clickResult(4);

		ingredients = await application.readIngredients();

		assert.deepEqual(ingredients, {'selection b': '?g',});

		await application.sendSearchKeys('selection');
		await application.getSearchResults(5);

		assert.deepEqual(await application.readSelected(), ['selection b']);

		await application.clickResult(1);

		ingredients = await application.readIngredients();

		assert.deepEqual(ingredients, {});

		await application.sendSearchKeys('selection');
		await application.getSearchResults(5);

		assert.deepEqual(await application.readSelected(), []);
	});
});
