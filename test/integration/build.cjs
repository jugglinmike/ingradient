'use strict';

const fs = require('fs');
const child_process = require('child_process');
const path = require('path');

const fixtureDataSource = path.join(__dirname, 'fixture-nutrition-data.json');
const fixtureDataDest = path.join(__dirname, '..', '..', 'dist', 'out.json');

const buildCode = () => {
	const child = child_process.spawn('npm', ['run', 'build']);
	let errorData = '';

	child.stderr.on('data', (chunk) => errorData += chunk);

	return new Promise((resolve, reject) => {
		child.on('close', (code) => {
			if (code !== 0) {
				reject(new Error(`Faild to build code: ${errorData}`));
				return;
			}
			resolve();
		});
	});
};

const insertFixtureData = () => {
	return new Promise((resolve, reject) => {
		fs.copyFile(fixtureDataSource, fixtureDataDest, (error) => {
			if (error) {
				reject(error);
				return;
			}
			resolve();
		});
	});
};

module.exports = () => {
	return buildCode().then(insertFixtureData);
};
