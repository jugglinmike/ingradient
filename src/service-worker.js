// jshint worker: true, strict: global
/* globals caches: true */
'use strict';

const cacheName = 'v1';

const handleFetch = async (event) => {
	const cache = await caches.open(cacheName);
	let response = await cache.match(event.request);

	if (!response) {
		response = await fetch(event.request);

		if (response.ok) {
			cache.put(event.request, response.clone());
		}

	// Always attempt to get latest HTML files
	} else if (/text\/html/i.test(response.headers.get('content-type'))) {
		try {
			response = await fetch(event.request);

			if (response.ok) {
				cache.put(event.request, response.clone());
			}
		// Work around bug in JSHint
		// https://github.com/jshint/jshint/issues/3480
		// jshint -W137
		} catch ({}) {}
		// jshint +W137
	}

	return response;
};

const handleActivate = async (event) => {
	const keys = await caches.keys();

	return Promise.all(keys
		.filter((key) => key !== cacheName)
		.map((key) => caches.delete(key))
	);
};

self.addEventListener('fetch', (event) => {
	event.respondWith(handleFetch(event));
});

self.addEventListener('activate', (event) => {
	event.waitUntil(handleActivate(event));
});
